import argparse
from math import sin, cos
import math
import pybullet as p
from time import sleep
from util import Point
import numpy as np

dt = 0.01

# Length (in mm)
l1 = 40
l2 = 45
l3 = 65
l4 = 87
# For test (inverse kinematics)
# l2 = 1.0
# l3 = 1.0
# l4 = 1.0

########################################################
#                        INITIALISE                    #
########################################################

def init():
    """Initialise le simulateur

    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF('plane.urdf')

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("./quadruped/robot.urdf", startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot

########################################################
#                        UTILS                         #
########################################################

def setJoints(robot, joints):
    """Définis les angles cibles pour les moteurs du robot

    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(
            robot, jointInfo[0], p.POSITION_CONTROL, joints[k])


def move_interpolation(t, tab ,tps):
    """
        Move to use interpolation

        Arguments:
        t {float} -- times (in second)
        tab {list} -- Position of all one leg of 5 positions
        tps {list} -- abscissa for 5 positions (use in interpolation)

        Returns: 
        {list} List of position of one leg (in t times)
    """
    abscisse=math.fmod(t,tps[-1])

    x_tab = [0] * len(tab)
    y_tab = [0] * len(tab)
    z_tab = [0] * len(tab)
    for i in range(len(tab)):
        x_tab[i], y_tab[i], z_tab[i] = tab[i]

    return inverse_kinematic(interpolate(tps,x_tab,abscisse),
                              interpolate(tps,y_tab,abscisse),
                              interpolate(tps,z_tab,abscisse)
                              )

def interpolate(xs, ys, x):
    """
        Interpolate linear function.

        Arguments:
        xs {list} -- multiple abscissa
        ys {list} -- multiple ordinate
        x {float} -- abscissa of one time

        Returns: 
        {float} ordinates of the element in abscissa
    """
    t=0
    for i in range(len(xs)-1):
        if x>xs[i]:
            t = i
    a = (ys[t+1] - ys[t]) / (xs[t+1] - xs[t])
    b = ys[t] - a * xs[t]
    return a* x +b


########################################################
#                 Inverse Kinematic                    #
########################################################


def resolve_math_domain_error(value):
    """
        Resolve math domain error for inverse kinematic when we use arcos et arcsin.

    Arguments:
        value {float} -- value must limit between -1 and 1 
    
    Returns:
        value -- the value between [-1,1]
    """
    if value < -1: 
        return -1
    elif value > 1:
        return 1
    else:
        return value

def inverse_kinematic(x, y, z):
    """Compute the inverse kinematic of a single leg.

    Arguments:
        x {float} -- Position en x (en m)
        y {float} -- Position en y (en m)
        z {float} -- Position en z (en m)
    
    Returns:
        list -- poses (in rad.) for the motors of a single leg.
    """
    # Create the point M
    M = Point(x,y,z)
    
    # Compute the first angle
    theta_0 = math.atan2(M.y,M.x)

    # Utils
    A = Point(l2* math.cos(theta_0), l2 * math.sin(theta_0), 0)
    AM = A.distance(M)

    # Compute the second angle
    
    omega = math.asin(z/AM) 
    value = (l3**2 + AM**2 - l4**2) / (2 * l3 * AM)
    value = resolve_math_domain_error(value)
 
    delta = math.acos(value)
    theta_1 = delta + omega

    # Compute the three angle
    value = (l3**2 + l4**2 - AM** 2) / (2 * l3 * l4)
    value = resolve_math_domain_error(value)
    
    alpha = -math.acos(value)
    theta_2 = alpha - math.pi 
    
    return [theta_0, theta_1, theta_2]


def compute_ik(leg_id, pos,extra_theta = 0):
    """
        Compute the new position in the leg_frame.

        Arguments:
        leg_id {int} -- id of leg
        pos {list} -- Position of the leg (in robot frame)
        extra_theta {list} -- theta to add in the robot (if we want the robot turns)

        Returns: 
        {list} Position of the leg (in leg frame)
    """
    # Calculate the angle between the axis of the robot body and the leg.
    theta = leg_id*math.pi/2 + math.pi/4
    # Add the rotation matrix and translate with the body (to turn).
    new_x = pos[0] * math.cos(theta) + pos[1] * math.sin(theta) +  2 * l1 * math.cos(extra_theta)
    new_y =  pos[1] * math.cos(theta) - pos[0] * math.sin(theta) -  2 * l1 * math.sin(extra_theta)
    return [new_x, new_y, pos[2]]

def prevent_collision(joints):
    for i in range(4):
        l_motor = 3 * i
        next_l_motor = (l_motor + 3) % 12
        ecart = math.pi / 9
        if (joints[l_motor] > math.pi/2 + joints[next_l_motor] - ecart):
            joints[l_motor] = math.pi/2 + joints[next_l_motor] - ecart
    return joints

########################################################
#                        DEMO                          #
########################################################

def demo(t, amplitude):
    """Démonstration de mouvement (fait osciller une patte)

    Arguments:
        t {float} -- Temps écoulé depuis le début de la simulation
        amplitude {float} -- Amplitude de l'oscillisation

    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
    """
    joints = [0]*12
    joints[0] = math.sin(t) * amplitude
    return joints

########################################################
#                   INITIAL POSITION                   #
########################################################

def init_pos():
    """Initialize the position.

    Returns:
        list -- poses for the 12 motors.
    """
    return robot_ik(0,0,-40)

########################################################
#                       LEG IK                         #
########################################################

def leg_ik(x, y, z, leg_id, joints):
    """Control the position of the tip of a single leg.
        
    Arguments:
        x {float} -- x position of the tip
        y {float} -- y position of the tip
        z {float} -- z position of the tip 
        leg_id {float} -- id of leg
    Returns:
        list -- 12 poses (in rad.) for the motors.
    """
    joints[leg_id*3 : leg_id * 3 + 3] = inverse_kinematic(x,y,z)
    return joints

########################################################
#                      ROBOT IK                        #
########################################################

def robot_ik(x,y,z):
    """
        Control position of body robot.

        Arguments:
        x {float} -- x position
        y {float} -- y position
        z {float} -- z position

        Returns: 
        {list} 12 poses (in rad.) for the motors.
    """
    joints = [0] * 12
    for i in range(4):
        news = compute_ik(i,[x,y,z], 0.0)
        joints[i*3:3+i*3] = inverse_kinematic(news[0], news[1], news[2])
    return joints

########################################################
#                        WALK                          #
########################################################

def walk(t, x_speed, y_speed, theta_speed):
    """
        Control speed of robot.

        Arguments:
        t {float} -- times
        x_speed {float} -- normal speed
        y_speed {float} -- tangent speed
        theta_speed {float} -- angular speed

        Returns: 
        {list} 12 poses (in rad.) for the motors.
    """
    # Create tabs
    leg = [0] * 4
    for i in range(0,4):
        leg[i] = [[0] * 3 for _ in range(5)] 

    ## m/s to mm/s
    depx = x_speed * 1000
    depy = y_speed * 1000

    ## mm/s to mm (0.01 dt is equal to have 100 period of one second)
    depx /= 20
    depy /= 20

    pos_dep_1 = [depx, depy, -50]
    pos_dep_2 = [depx, depy, -40]

    neg_dep_1 = [-depx, -depy, -50]
    neg_dep_2 = [-depx, -depy, -40]

    # Populate tabs
    # Leg 0 and 2
    for i in [0,2] :
        leg[i][0] = leg[i][4] = compute_ik(i, neg_dep_1,-theta_speed)
        leg[i][1] = compute_ik(i, neg_dep_2,-theta_speed)
        leg[i][2] = compute_ik(i, pos_dep_2,theta_speed)
        leg[i][3] = compute_ik(i, pos_dep_1 ,theta_speed)
    
    # Leg 1 and 3
    for i in [1,3] :
        leg[i][0] = leg[i][4] = compute_ik(i,pos_dep_2,theta_speed)
        leg[i][1] = compute_ik(i,pos_dep_1,theta_speed)
        leg[i][2] = compute_ik(i,neg_dep_2,-theta_speed)
        leg[i][3] = compute_ik(i,neg_dep_1,-theta_speed)
    
    tps = [0 + 0.25 * j for j in range(5)]

    joints = [0] * 12
    for i in range (4) :
        joints[i*3:3+i*3] = move_interpolation(t,leg[i],tps)
    return joints

########################################################
#                        GO TO                         #
########################################################

r_x, r_y, r_t = 0, 0, 0
finish = False

def goto(t,x,y,theta):
    """
        Go To in coordinate

        Arguments:
        t {float} -- times
        x {float} -- x position
        y {float} -- y position
        theta_speed {float} -- theta position

        Returns: 
        {list} 12 poses (in rad.) for the motors.
    """
    global r_x, r_y, r_t, finish

    # Block for remove some error and derivation (can replace by a PID and an odemetry)    
    fact_error_x, fact_error_y = 0, 0
    mult = 100.0
    if x != 0 and y != 0 :
        mult = 115
        fact_error_x = - 0.007
        fact_error_y =  0.005
    elif y != 0 :
        fact_error_x = - 0.007
    else:
        fact_error_y = 0.0005
    
    dist = Point(x * mult , y * mult, 0.0).distance(Point(r_x, r_y, 0.0))
    remaining_x = x * mult - r_x
    remaining_y = y * mult - r_y

    if dist > 0.4 :
        speed = 0.4
    elif dist < 0.1:
        speed = 0.0
    else: 
        speed = 0.1

    if speed == 0.0:
        x_speed = 0
        y_speed = 0
        t_speed = 0
        if(not(finish)):
            t_speed = math.pi / 12
            t_error = 0
            finish = (theta - r_t) < 0
    else:  
        x_speed = (remaining_x / dist) * speed
        y_speed = (remaining_y / dist) * speed
        t_speed = 0
    
    if finish: 
        joints = init_pos()
    else: 
        joints = walk(t,x_speed, y_speed,t_speed)
        r_x = r_x + x_speed * 0.20 + y_speed * fact_error_x
        r_y = r_y + y_speed * 0.20 - x_speed * fact_error_y 
        r_t = r_t + t_speed * 0.055

    return joints

########################################################
#                        FUN                           #
########################################################

def compute_fun(i, x, y, z):
    """
        Compute legs for fun 1 (3 leg in the ground and one in the air).

        Arguments:
        t {float} -- times
        x {float} -- x position of the leg in the air
        y {float} -- y position of the leg in the air
        speed {float} -- z position of the leg in the air

        Returns: 
        {list} 12 poses (in rad.) for the motors.
    """
    joints[0] * 12
    news = compute_ik(i,[x,y,z], 0.0)
    joints[i*3:i*3+3] = inverse_kinematic(x,y,z)
    for j in range(4):
        if j != i:
            joints[j*3:j*3+3] = inverse_kinematic(0, 0, -40)
    return joints


def step(t):
    """
        Playing step.

        Arguments:
        t {float} -- times

        Returns: 
        {list} 12 poses (in rad.) for the motors.
    """
    period_of_frame = 1
    frame = (t / period_of_frame) % 8
    leg = int(frame / 2)
    if(frame % 2 < 1):
        return compute_fun(leg,180.0, 0, 100.0)
    else:
        return robot_ik(0,0,-40)

i = 0
sign = -1
def push_up(t):
    """
        Do push-ups

        Arguments:
        t {float} -- times

        Returns: 
        {list} 12 poses (in rad.) for the motors.
    """
    global i, sign
    joints = robot_ik(i,0, -40)
    i = i + sign
    if i < -140:
        sign = 1
    elif i > 0:
        sign = -1
    return joints

########################################################
#                        MAIN                          #
########################################################

if __name__ == "__main__":
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument(
        '-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument(
        '-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument(
        '-t', type=float, help='Theta target for goto (rad)', default=0.0)
    parser.add_argument(
        '-f', type=bool, help='run fun 1 (true) and fun 2 (false)', default=False)
    args = parser.parse_args()

    mode, x, y, theta, choose_fun_1 = args.m, args.x, args.y, args.t, args.f

    if mode not in ['demo', 'leg_ik', 'robot_ik', 'walk', 'goto', 'fun']:
        print('Le mode %s est inconnu' % mode)
        exit(1)

    robot = init()
    if mode == 'demo':
        
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de démonstration...')

    elif mode == 'leg_ik':
        print("leg_ik mode")

        slider_x = p.addUserDebugParameter("x",  -200, 200, 0)
        slider_y = p.addUserDebugParameter("y", -200, 200, 0)
        slider_z = p.addUserDebugParameter("z", -100, 100, -40)

        # For test (kinematics with 1,1,1 mm and legs and see if it is good)
        # slider_x = p.addUserDebugParameter("x",  0, 19.6, 2)
        # slider_y = p.addUserDebugParameter("y", -0.8, 0.8, 0)
        # slider_z = p.addUserDebugParameter("z", -20, 20, -1)
        
        slider_patte = p.addUserDebugParameter("patte_id", 0,3,0)
    elif mode == 'robot_ik':
        print("robot_ik mode")
        slider_x = p.addUserDebugParameter("x",  -140, 140, 0)
        slider_y = p.addUserDebugParameter("y", -140, 140, 0)
        slider_z = p.addUserDebugParameter("z", -140, 140, -40)
    elif mode == 'walk':
        print("walk mode")
        x_speed = p.addUserDebugParameter("x_speed",  -3, 3, 0.0)
        y_speed = p.addUserDebugParameter("y_speed",  -3, 3, 0.0)
        t_speed = p.addUserDebugParameter("theta_speed",  -math.pi / 2, math.pi / 2, 0)
    elif mode == 'goto':
        print("go to mode")
        # always theta positive
        if theta < 0:
            theta = theta % (2 * math.pi)
    elif mode == 'fun':
        print("fun mode")
    else:
        raise Exception('Mode non implémenté: %s' % mode)

    t = 0

    joints = [0] * 12
    # Boucle principale
    while True:
        t += dt

        if t<1:
            joints = init_pos()
        elif mode == 'demo':
            # Récupération des positions cibles
            joints = demo(t, p.readUserDebugParameter(amplitude))
        elif mode == 'leg_ik':
            joints = leg_ik(p.readUserDebugParameter(slider_x), 
                            p.readUserDebugParameter(slider_y), 
                            p.readUserDebugParameter(slider_z),
                            int(np.rint(p.readUserDebugParameter(slider_patte)).item()), joints)
        elif mode == 'robot_ik':
            joints = robot_ik(-p.readUserDebugParameter(slider_x),
                              p.readUserDebugParameter(slider_y), 
                              p.readUserDebugParameter(slider_z)
            )
        elif mode == 'walk':
            joints = walk(t-1,
                          p.readUserDebugParameter(x_speed),
                          -p.readUserDebugParameter(y_speed),
                          p.readUserDebugParameter(t_speed),
                          )
        elif mode == 'goto':
            joints = goto(t,x,-y,theta)
        elif mode == 'fun':
            if choose_fun_1:
                joints = step(t)
            else:
                joints = push_up(t)
        joints = prevent_collision(joints)
        

        # Envoi des positions cibles au simulateur
        setJoints(robot, joints)

        # Mise à jour de la simulation
        p.stepSimulation()
        sleep(dt)
