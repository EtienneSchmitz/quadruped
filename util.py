import math

class Point:
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z

    def distance(self,p):
        return math.sqrt(math.pow(self.x - p.x, 2) + math.pow(self.y - p.y, 2) + math.pow(self.z - p.z, 2))