# Quadruped

Dépendances à installer:

    pip install numpy scipy pybullet jupyter matplotlib

Ensuite:

    python quadruped.py -m mode [-x x-cible -y y-cible -t t-cible]

# Consignes :

Vous réaliserez un programme (script) python, doté de plusieurs modes:

- demo: un mode de démo, déjà présent dans le code de base
- leg_ik: permet de contrôler la position du bout d'une seule patte, ce mode proposera 3 sliders (x, y, z) dans PyBullet et permet de contrôler la position du bout de cette patte
    + Pensez à gérer tous les cas de figure sachant que le nombre de solutions pour un point demandé en cinématique inverse peut être 0, 1, 2 ou infini.
    + Expliquez dans votre README.md comment vous avez géré ces cas (explication synthétique suffisante)
- robot_ik: permet de contrôler la position du corps du robot, en proposant également 3 sliders (x, y, z)
- walk: ce mode propose des sliders x_speed, y_speed et t_speed, respectivement en m/s, m/s et rad/s, qui permettent de contrôler la vitesse de la marche du robot (note: pensez à utiliser l'interpolation faite en cours...)
- goto: en utilisant la marche, le robot doit se rendre sur la position cible en utilisant les arguments -x, -y et -t (en m, m et radian)
- fun: libérez votre créativité (https://youtu.be/71E2WYxozgU)

Quelques objectifs indépendants des modes :

- Quoiqu'il arrive, le robot ne doit jamais faire de mouvements brusques
- Définissez une position stable de référence où seuls les bouts des pattes touchent le sol. Quel que soit le mode, le robot doit commencer par atteindre cette position stable
- Implémentez des limites d'angles sur les articulations pour éviter les auto-collisions évidentes
(gros) bonus si vous gérez l'auto-collision dans le cas général. Attention, c'est un sujet avancé à ne faire que si le reste est fonctionnel



## Explication du travail

Pour gérer les singularité, on fixe un premier moteur dans un premier temps (celui le plus proche de la patte.

Comme le robot avait envie de faire un peu sport (vu que lui est aussi en confinement), je lui en ai donné la possibilité via deux autres modes (en bonus) :
- Pompe : faire python quadruped.py -m fun
- Step : faire python quadruped.py -m fun -f True

Remarque : 
- J'ai inversé le y envoyé dans les paramètres des fonctions go to et le walk comme le repère du robot et le repère de pybullet était inversé 